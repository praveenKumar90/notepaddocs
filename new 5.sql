SELECT COUNT(k5.id_c) as kharif_2075_Girdawari,  COUNT(k6.id_c) as kharif_2076_Girdawari, COUNT(r5.id_c) as rabi_2075_Girdawari, COUNT(z6.id_c) as zaid_2076_Girdawari
FROM lrs.m_p_13_form_main_cstm k5
JOIN kharif_2076.m_p_13_form_main_cstm k6 ON k6.m_patwari_id_c = k5.m_patwari_id_c
JOIN rabi_2075.m_p_13_form_main_cstm r5 ON r5.m_patwari_id_c = k6.m_patwari_id_c
JOIN zaid_2076.m_p_13_form_main_cstm z6 ON z6.m_patwari_id_c = r5.m_patwari_id_c
WHERE k5.m_patwari_id_c = "74aee46e-d204-038e-1ce1-7f377552c581";

==================================
check girdawari
==================================
SELECT t1.id_c, t1.patwari_name_c, t1.phone_c, t1.patwar_circle_c, t1.govt_id_c, t1.ssoId_c, 
COUNT(t2.id_c) as kharif_2076_girdawari, 
COUNT(t3.id_c) as kharif_2075_girdawari, 
COUNT(t4.id_c) as rabi_2075_girdawari, 
COUNT(t5.id_c) as zaid_2076_girdawari
FROM lrs.m_patwari_cstm t1 
LEFT JOIN kharif_2076.m_p_13_form_main_cstm t2 ON t2.m_patwari_id_c = t1.id_c
LEFT JOIN lrs.m_p_13_form_main_cstm t3 ON t3.m_patwari_id_c = t1.id_c
LEFT JOIN rabi_2075.m_p_13_form_main_cstm t4 ON t4.m_patwari_id_c = t1.id_c
LEFT JOIN zaid_2076.m_p_13_form_main_cstm t5 ON t5.m_patwari_id_c = t1.id_c
WHERE t1.id_c = "e43496f8-7639-7f06-62cb-22999a184fb3";

================================
check village data
================================
select * FROM kharif_2076.khasra_count WHERE geo_id IN 
(SELECT id_c FROM lrs.`online_geo_data_cstm` WHERE `i_geohierarchy_id_c` LIKE '56c13848-b999-55d0-fa5b-e57018553eed');

SELECT COUNT(*) FROM kharif_2076.girdawari_detail WHERE village_id IN
(SELECT id_c FROM lrs.`online_geo_data_cstm` WHERE `i_geohierarchy_id_c` LIKE '56c13848-b999-55d0-fa5b-e57018553eed');

SELECT COUNT(*) FROM kharif_2076.owner_details WHERE internal_code IN
(SELECT internal_code FROM lrs.`online_geo_data_cstm` WHERE `i_geohierarchy_id_c` LIKE '56c13848-b999-55d0-fa5b-e57018553eed');