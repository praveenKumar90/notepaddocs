id:  e5320e18-894a-5e9e-f291-85842c2d5560

name:  तुर्सीपुरा

geotype_c:  patwar

patwar circle   i_geohierarchy_id_c:   4caadec0-1c84-4964-a911-ba10bdbe5763


========================================================================
add patwar:


INSERT INTO lrs.online_geo_data(id, name, date_entered, date_modified, deleted) VALUES ('e5320e18-894a-5e9e-f291-85842c2d5560', 'तुर्सीपुरा', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0');

INSERT INTO lrs.online_geo_data_cstm(id_c, creation_date_c, geotype_c, i_geohierarchy_id_c, district_census_c, tehsil_census_c, is_syncing)
VALUES('e5320e18-894a-5e9e-f291-85842c2d5560', CURRENT_TIMESTAMP, 'patwar', '4caadec0-1c84-4964-a911-ba10bdbe5763', '106', '00515', '0');

=================================================================================================

============================================================================================================================
add village

INSERT INTO lrs.online_geo_data(id, name, date_entered, date_modified, deleted) VALUES ('e5320e18-894a-5c9c-f291-85842c2d5560', 'तुर्सीपुरा', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0');

INSERT INTO lrs.online_geo_data_cstm(id_c, creation_date_c, geotype_c, i_geohierarchy_id_c, district_census_c, tehsil_census_c, is_syncing, internal_code)
VALUES('e5320e18-894a-5c9c-f291-85842c2d5560', CURRENT_TIMESTAMP, 'village', 'e5320e18-894a-5e9e-f291-85842c2d5560', '106', '00515', '0', '16922');

======================================================================================================================================
get online tehsil before date:

SELECT t4.name, t2.district_census_c, t2.tehsil_census_c 
FROM lrs.online_geo_data t1
JOIN lrs.online_geo_data_cstm t2 ON id_c = id
JOIN lrs.online_geo_data_cstm t3 ON t3.census_number_c = t2.tehsil_census_c AND t3.geotype_c = 'tehsil'
JOIN lrs.online_geo_data t4 ON t4.id = t3.id_c
WHERE t2.geotype_c = 'village' 
AND t2.tehsil_census_c != ''
AND DATE_FORMAT(t1.date_entered, "%m-%Y") <= "10-2019"
GROUP BY t2.district_census_c, t2.tehsil_census_c;