select d.distname_Eng,count(t.tehName_Eng) from DistrictMaster d join TehsilMaster t on t.Mapcode like "%"+d.Mapcode+"__%" group by d.distname_Eng;

select d.distname_Eng,t.tehName_Eng,v.villname_Eng from DistrictMaster d join TehsilMaster t on t.Mapcode like "%"+d.Mapcode+"__%" join VillageMaster v on v.Mapcode
like "%"+t.Mapcode+"0___%" ;

select tab1.name,
count(distinct tab2.id) as tab2_record_count
count(distinct tab3.id) as tab3_record_count
count(distinct tab4.id) as tab4_record_count
from tab1
left join tab2 on tab2.tab1_id = tab1.id
left join tab3 on tab3.tab1_id = tab1.id
left join tab4 on tab4.tab1_id = tab1.id

//////////////////////////////////////////////////////////////////////////////////////////////////////////
check patwari details:

SELECT `id_c`, `patwari_name_c`, `gender_c`, `email_c`, `patwar_circle_c`, `phone_c`,`govt_id_c`, `app_version_c`, `have_problem_c`, `version_c`, `ssoId_c`, `deleted`
FROM lrs.`m_patwari_cstm` JOIN lrs.m_patwari ON id = id_c
WHERE `phone_c` LIKE '8386868355';

SELECT `id_c`, `patwari_name_c`, `gender_c`, `email_c`, `patwar_circle_c`, `phone_c`,`govt_id_c`, `app_version_c`, `have_problem_c`, `version_c`, `ssoId_c`, `deleted`
FROM lrs.`m_patwari_cstm` JOIN lrs.m_patwari ON id = id_c
WHERE `phone_c` LIKE '9784003641';

================================================================================================================================
select v.ilrCode, i.ilrName, v.patwarCode, p.patwarName from VillageMap v 
JOIN ILRMaster i ON i.ilrCode = v.ilrCode
JOIN PatwarMaster p ON p.patwarCode = v.patwarCode WHERE
v.distCode = '23' AND v.tehsCode = '255'

=====================================================================================================================================
GET village data:

select i.ilrCode, i.ilrName, p.patwarCode, p.patwarName, vm.villCode, vm.villname FROM
VillageMap v JOIN DistrictMaster d ON d.distCode = v.distCode
JOIN TehsilMaster t ON t.tehCode = v.tehsCode
JOIN ILRMaster i ON i.ilrCode = v.ilrCode
JOIN PatwarMaster p ON p.patwarCode = v.patwarCode
JOIN VillageMaster vm ON vm.villCode = v.villCode
WHERE v.tehsCode = '25' and v.status = 'C'
group by i.ilrCode, i.ilrName, p.patwarCode, p.patwarName, vm.villCode, vm.villname 
order by i.ilrName;
_______________________________________________________________OR_____________________________________________________________________

select CONCAT('"',p.patwarCode,'" =>["villCode" => "',vm.villCode,'", "villName" => "',vm.villName,'", "Census" =>"',REPLACE(v.censuscode, ' ', ''),'"]') FROM
VillageMap v JOIN DistrictMaster d ON d.distCode = v.distCode
JOIN TehsilMaster t ON t.tehCode = v.tehsCode
JOIN ILRMaster i ON i.ilrCode = v.ilrCode
JOIN PatwarMaster p ON p.patwarCode = v.patwarCode
JOIN VillageMaster vm ON vm.villCode = v.villCode
WHERE v.tehsCode = '25' and v.status = 'C'
group by p.patwarCode, vm.villCode, vm.villname, v.censuscode 
order by p.patwarCode, vm.villname;