check ilr and patwar by district and tehsil censuscode
========================================================================================================================================================================

SELECT t.tehName,i.ilrName, p.patwarName, vm.villname  FROM VillageMap v 
JOIN DistrictMaster d ON d.distCode = v.distCode
JOIN TehsilMaster t ON t.tehCode = v.tehsCode 
JOIN ILRMaster i ON i.ilrCode = v.ilrCode
JOIN PatwarMaster p ON p.patwarCode = v.patwarCode
JOIN VillageMaster vm ON vm.villCode = v.villCode
WHERE d.censuscode = '125' and t.censuscode = '00651' and v.status = 'C' order by t.tehName,i.ilrName, p.patwarName, vm.villname;

========================================================================================================================================================================
SELECT count(distinct concat(g.gkhata, g.khasra2)) as khasre, vm.villname FROM VillageMap v 
JOIN DistrictMaster d ON d.distCode = v.distCode
JOIN TehsilMaster t ON t.tehCode = v.tehsCode
JOIN VillageMaster vm ON vm.villCode = v.villCode
JOIN Jaipur.dbo.GeneralNew g ON g.vill_link = v.vill_link
WHERE d.censuscode = '103' and t.censuscode = '00492' and v.status = 'C' group by vm.villname;

========================================================================================================================================================================
get online tehsil with district: 

SELECT d.distname, t.tehname FROM VillageMap v 
JOIN DistrictMaster d ON d.distCode = v.distCode
JOIN TehsilMaster t ON t.tehCode = v.tehsCode
WHERE v.status = 'C'
group by d.distname, t.tehname
order by d.distname, t.tehname;

========================================================================================================================================================================
get total khasre and owners by villCode:

SELECT v.villCode, v.vill_link, vm.villname, count(distinct g.khasra2) as total_khasre, count(o.Owner_ID) as owners  FROM [dbo].[VillageMap] v
JOIN [dbo].[VillageMaster] vm ON vm.villCode = v.villCode
JOIN [Jodhpur].[dbo].[GeneralNew] g ON g.vill_link = v.vill_link
JOIN [Jodhpur].[dbo].[OwnerMaster] o ON o.vill_link = v.vill_link
WHERE v.[villCode] IN (20343,20344,20370) and v.status = 'C' group by  v.villCode, v.vill_link, vm.villname order by v.villCode;

========================================================================================================================================================================
nic village data:

SELECT db, vill_link,villCode FROM VillageMap WHERE villCode = '20344' OR censuscode = '20344';

SELECT DISTINCT om.OwnerNo uniqueno,
osd.OKhata khata, 
om.Oname name,
om.oFather father,
om.oHusband husband,
mr.relationname relationname
FROM Jodhpur.dbo.OwnerMaster om 
JOIN Jodhpur.dbo.OwnerShareDetl osd ON osd.OwnerNo = om.OwnerNo 
LEFT JOIN portallrc.dbo.MstRelation mr ON mr.relationid = om.relation
where om.Vill_link = '25430';
========================================================================================================================================================================

========================================================================================================================================================================

========================================================================================================================================================================

========================================================================================================================================================================

========================================================================================================================================================================