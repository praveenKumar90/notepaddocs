/*find the village, patwar, ilr, tehsil, district
========================================================================================================================================================================*/
SELECT t1.id, t1.name, t2.geotype_c, t2.i_geohierarchy_id_c 
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id WHERE name LIKE "रामगढ" AND t2.district_census_c = '125' AND t2.tehsil_census_c = '00654';


SELECT t1.id, t1.name, t2.geotype_c, t2.i_geohierarchy_id_c,
if(1, (SELECT name FROM lrs.online_geo_data WHERE id = t2.i_geohierarchy_id_c), 0) as top_approach
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id WHERE name LIKE "खीमाङा";


SELECT t1.id, CONCAT(t2.geotype_c, " = ", t1.name) as name, t2.i_geohierarchy_id_c,
if(1, (SELECT CONCAT(geotype_c, " = ", name) FROM lrs.online_geo_data JOIN lrs.online_geo_data_cstm ON id_c = id WHERE id = t2.i_geohierarchy_id_c), 0) as top_approach
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id WHERE 
name LIKE "कठूमर";

/*========================================================================================================================================================================
reverse process for village ----- ilr  or  ilr ---- village
========================================================================================================================================================================*/

SELECT t1.id, t1.name, t2.geotype_c, t2.i_geohierarchy_id_c 
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
WHERE t2.i_geohierarchy_id_c = "138ce589-a426-4bc5-bbee-eab6c1337639";


SELECT t2.i_geohierarchy_id_c as id1, t1.id as id2, g2.id as id3, if(1, (SELECT name FROM lrs.online_geo_data WHERE id = t2.i_geohierarchy_id_c), 0) as name1,
t1.name as name2, g2.name as name3
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
JOIN lrs.online_geo_data_cstm g1 ON g1.i_geohierarchy_id_c = t2.id_c
JOIN lrs.online_geo_data g2 ON g2.id = g1.id_c
WHERE t2.i_geohierarchy_id_c = "0ddc0458-d087-5211-ca45-b1aed5261165" ORDER BY t1.name, g2.name;

/*========================================================================================================================================================================
check village data*/

SELECT t1.id, t1.name, t2.geotype_c, t2.i_geohierarchy_id_c, t3.khasra_count, t3.geo_status,
count(DISTINCT t4.id) as girdawari, 
count(t5.id) as owners
FROM lrs.online_geo_data t1 
JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
LEFT JOIN kharif_2076.khasra_count t3 ON t3.geo_id = t1.id
LEFT JOIN kharif_2076.girdawari_detail t4 ON t4.village_id = t1.id
LEFT JOIN kharif_2076.owner_details t5 ON t5.internal_code = t2.internal_code
WHERE t2.i_geohierarchy_id_c = "363dc477-05ca-426b-a02f-d50a41029b6b" GROUP BY t1.id;

/*========================================================================================================================================================================
check girdawari:*/

SELECT t1.name, t2.geotype_c, t3.khasra_count as total_khasre, count(distinct t4.khasra_number_c) as total_girdawari
FROM
lrs.online_geo_data t1
LEFT JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id
LEFT JOIN kharif_2076.khasra_count t3 ON t3.geo_id = t1.id 
LEFT JOIN kharif_2076.m_p_13_form_main_cstm t4 ON t4.i_geohierarchy_id_c = t1.id
WHERE t2.i_geohierarchy_id_c = "a14957a2-a3be-4754-92c1-7f95bdd6c1a2" GROUP BY t4.i_geohierarchy_id_c;

/*========================================================================================================================================================================
check girdawari by patwar circle:*/

SELECT count(t1.id_c) as total_girdawari_kharif_2076, count(t4.id_c) as total_girdawari_kharif_2075, count(t5.id_c) as total_girdawari_rabi_2075, 
count(t6.id_c) as total_girdawari_zaid_2076, t2.name, t3.geotype_c
FROM
kharif_2076.m_p_13_form_main_cstm t1 JOIN lrs.online_geo_data t2 ON t2.id = t1.i_geohierarchy_id_c
LEFT JOIN lrs.m_p_13_form_main_cstm t4 ON t4.i_geohierarchy_id_c = t2.id
LEFT JOIN rabi_2075.m_p_13_form_main_cstm t5 ON t5.i_geohierarchy_id_c = t2.id
LEFT JOIN zaid_2076.m_p_13_form_main_cstm t6 ON t6.i_geohierarchy_id_c = t2.id
JOIN lrs.online_geo_data_cstm t3 ON t3.id_c = t2.id WHERE t3.i_geohierarchy_id_c = "2347cf03-4d2f-e13a-3a3f-64685903e2fb" GROUP BY t1.i_geohierarchy_id_c;
/*========================================================================================================================================================================
check girdawari by patwar according to village:*/

SELECT COUNT(t1.id_c), t2.id, t2.name, t3.i_geohierarchy_id_c, t3.geotype_c, t1.m_patwari_id_c
FROM kharif_2076.m_p_13_form_main_cstm t1 JOIN lrs.online_geo_data t2 ON t2.id = t1.i_geohierarchy_id_c 
JOIN lrs.online_geo_data_cstm t3 ON t3.id_c = t2.id
WHERE t3.i_geohierarchy_id_c = "8a2b03ae-0ea3-d2c2-1688-feabfa45e9ad" GROUP BY t1.i_geohierarchy_id_c;

/*========================================================================================================================================================================

========================================================================================================================================================================
check girdawari and village data by village id:*/

SELECT t.id, t.name, k.khasra_count, k.geo_status, COUNT(DISTINCT g.khasra) as village_girdawari_data, COUNT(DISTINCT m.khasra_number_c) as done_girdawari, COUNT(o.id) as owners
FROM
lrs.online_geo_data t JOIN lrs.online_geo_data_cstm tt ON tt.id_c = t.id
LEFT JOIN kharif_2076.khasra_count k ON k.geo_id = t.id
LEFT JOIN kharif_2076.girdawari_detail g ON g.village_id = t.id
LEFT JOIN kharif_2076.owner_details o ON o.internal_code = tt.internal_code
LEFT JOIN kharif_2076.m_p_13_form_main_cstm m ON m.i_geohierarchy_id_c = t.id
WHERE t.id = "a3eb1ab5-a706-462c-bb97-409daeb8f186";
/*========================================================================================================================================================================
check girdawari and khasra_count:*/


SELECT t1.id, t2.i_geohierarchy_id_c, CONCAT(t2.geotype_c, " = ", t1.name) as name, t3.khasra_count, COUNT(DISTINCT t4.khasra_number_c) as done_girdawari,
if(1, (SELECT CONCAT(geotype_c, " = ", name) FROM lrs.online_geo_data JOIN lrs.online_geo_data_cstm ON id_c = id WHERE id = t2.i_geohierarchy_id_c), 0) as top_approach
FROM 
lrs.online_geo_data t1 LEFT JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
LEFT JOIN kharif_2076.khasra_count t3 ON t3.geo_id = t1.id 
LEFT JOIN kharif_2076.m_p_13_form_main_cstm t4 ON t4.i_geohierarchy_id_c = t1.id WHERE 
t2.i_geohierarchy_id_c = "8a2b03ae-0ea3-d2c2-1688-feabfa45e9ad" GROUP BY t1.id;

SELECT t1.id, t1.name, t3.khasra_count as total_khasre, COUNT(DISTINCT CONCAT(t4.khata_number_c, t4.khasra_number_c)) as done_girdawari FROM
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id
LEFT JOIN kharif_2076.khasra_count t3 ON t3.geo_id = t1.id
LEFT JOIN kharif_2076.m_p_13_form_main_cstm t4 ON t4.i_geohierarchy_id_c = t1.id 
WHERE t2.i_geohierarchy_id_c = "70fad9c3-1a5d-49c4-b1e2-9fef7229c28e" GROUP BY t1.id;

/*========================================================================================================================================================================
get all villages by tehsil id:*/

SELECT if(1, (SELECT name FROM lrs.online_geo_data WHERE id = t2.i_geohierarchy_id_c), 0) as Tehsil,
t1.name as ILR, g2.name as Patwar, c2.name as Village
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
JOIN lrs.online_geo_data_cstm g1 ON g1.i_geohierarchy_id_c = t2.id_c
JOIN lrs.online_geo_data g2 ON g2.id = g1.id_c
JOIN lrs.online_geo_data_cstm c1 ON c1.i_geohierarchy_id_c = g1.id_c
JOIN lrs.online_geo_data c2 ON c2.id = c1.id_c
WHERE t2.i_geohierarchy_id_c = "03a1fef1-e360-4414-8d7e-2d148c214b92" ORDER BY Tehsil, ILR, Patwar, Village;




/*========================================================================================================================================================================*/
SELECT if(1, (SELECT name FROM lrs.online_geo_data WHERE id = t2.i_geohierarchy_id_c), 0) as Tehsil,
t1.name as ILR, g2.name as Patwar, c2.name as Village, COUNT(DISTINCT CONCAT(g.khata_number_c,"_", g.khasra_number_c)) as girdawari, k.khasra_count as total_khasre
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
JOIN lrs.online_geo_data_cstm g1 ON g1.i_geohierarchy_id_c = t2.id_c
JOIN lrs.online_geo_data g2 ON g2.id = g1.id_c
JOIN lrs.online_geo_data_cstm c1 ON c1.i_geohierarchy_id_c = g1.id_c
JOIN lrs.online_geo_data c2 ON c2.id = c1.id_c
LEFT JOIN kharif_2076.m_p_13_form_main_cstm g ON g.i_geohierarchy_id_c = c2.id
LEFT JOIN kharif_2076.khasra_count k ON k.geo_id = c2.id
WHERE t2.i_geohierarchy_id_c = "03a1fef1-e360-4414-8d7e-2d148c214b92" GROUP BY c2.id ORDER BY Tehsil, ILR, Patwar, Village;

/*========================================================================================================================================================================
khasra_reporting: by tehsil_name*/

SELECT c2.id, c2.name as Village, k.khasra_count as khasra, dg.done_khasra
FROM
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
JOIN lrs.online_geo_data_cstm g1 ON g1.i_geohierarchy_id_c = t2.id_c
JOIN lrs.online_geo_data g2 ON g2.id = g1.id_c
JOIN lrs.online_geo_data_cstm c1 ON c1.i_geohierarchy_id_c = g1.id_c
JOIN lrs.online_geo_data c2 ON c2.id = c1.id_c
LEFT JOIN kharif_2076.khasra_count k ON k.geo_id = c2.id
LEFT JOIN kharif_2076.done_khasra_reporting dg ON dg.i_geohierarchy_id_c = c2.id
WHERE t2.i_geohierarchy_id_c = "03a1fef1-e360-4414-8d7e-2d148c214b92" ORDER BY Village;

/*========================================================================================================================================================================
get patwar using tehsil:*/

SELECT g2.id, if(1, (SELECT name FROM lrs.online_geo_data WHERE id = t2.i_geohierarchy_id_c), 0) as name1,
t1.name as name2, g2.name as name3, t1.deleted, g2.deleted, t2.district_census_c, t2.tehsil_census_c
FROM 
lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
JOIN lrs.online_geo_data_cstm g1 ON g1.i_geohierarchy_id_c = t2.id_c
JOIN lrs.online_geo_data g2 ON g2.id = g1.id_c
WHERE t2.i_geohierarchy_id_c = "0ddc0458-d087-5211-ca45-b1aed5261165" ORDER BY t1.name;
/*========================================================================================================================================================================

get all patwar and patwari detail by district id:*/

SELECT t6.name as tehsil, t5.name as ilr, t1.name as patwar, p.patwari_name_c as name, p.user_type_c as user FROM
lrs.online_geo_data t1 
JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id
JOIN lrs.online_geo_data_cstm t3 ON t3.id_c = t2.i_geohierarchy_id_c
JOIN lrs.online_geo_data t5 ON t5.id = t3.id_c
JOIN lrs.online_geo_data_cstm t4 ON t4.id_c = t3.i_geohierarchy_id_c
JOIN lrs.online_geo_data t6 ON t6.id = t4.id_c
JOIN lrs.m_patwari_cstm p ON p.patwar_circle_c LIKE CONCAT("%",t1.id,"%")
WHERE t4.i_geohierarchy_id_c = "12c10e82-6c30-4205-a5ab-b99c13f50950" GROUP BY t1.name ORDER BY t1.name;
/*========================================================================================================================================================================

check how many patwars selected by an patwari:*/

SELECT patwari_name_c, JSON_LENGTH(JSON_EXTRACT(patwar_circle_c, "$.patwars")) as tehsil
FROM lrs.`m_patwari_cstm` WHERE `patwar_circle_c` NOT IN ('{"patwars":[]}') AND `user_type_c` IN ('sdm') order by patwari_name_c;

/*========================================================================================================================================================================
check girdawari reporting district wise:*/

SELECT  t8.name as tehsil, t6.name as ilr, t5.name as patwar , t1.name as village, p.patwari_name_c as name, p.user_type_c as user,
IFNULL(SUM(k.khasra_count),0) as total_khasre,  IFNULL(SUM(g.done_khasra),0) as done_girdawari
FROM lrs.online_geo_data t1 
JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id
JOIN lrs.online_geo_data_cstm t3 ON t3.id_c = t2.i_geohierarchy_id_c
JOIN lrs.online_geo_data t5 ON t5.id = t3.id_c
JOIN lrs.online_geo_data_cstm t4 ON t4.id_c = t3.i_geohierarchy_id_c
JOIN lrs.online_geo_data t6 ON t6.id = t4.id_c
JOIN lrs.online_geo_data_cstm t7 ON t7.id_c = t4.i_geohierarchy_id_c
JOIN lrs.online_geo_data t8 ON t8.id = t7.id_c
JOIN lrs.m_patwari_cstm p ON p.patwar_circle_c LIKE CONCAT("%",t5.id,"%")
LEFT JOIN kharif_2076.khasra_count k ON k.geo_id = t1.id
LEFT JOIN kharif_2076.done_khasra_reporting g ON g.m_patwari_id_c = p.id_c AND g.i_geohierarchy_id_c = t1.id
WHERE t7.i_geohierarchy_id_c = "12c10e82-6c30-4205-a5ab-b99c13f50950" GROUP BY t1.name ORDER BY t8.name, t6.name, t5.name, t1.name;

/*========================================================================================================================================================================
get tehsil:*/

SELECT t1.name, t1.date_entered FROM lrs.online_geo_data t1 JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id WHERE
t2.geotype_c = 'tehsil' AND DATE_FORMAT(t1.date_entered, "%m-%Y") > "10-2019" AND t1.deleted = '0' order by t1.date_entered;

/*========================================================================================================================================================================
check tehsil wise girdawari status:*/

Select OG.district_census_c As distId, OG.tehsil_census_c AS id, 
IFNULL(SUM(KC.khasra_count),0) kul_khasra, 
IFNULL(SUM(DR.done_khasra),0) doneKhasra, 
SUM(CASE WHEN IFNULL(DR.done_khasra,0) > IFNULL(KC.khasra_count,0) THEN IFNULL(KC.khasra_count,0) ELSE IFNULL(DR.done_khasra,0) END) as totalKhasra 
from lrs.online_geo_data_cstm OG 
JOIN lrs.online_geo_data DE ON DE.id = OG.id_c
LEFT JOIN kharif_2076.khasra_count KC ON KC.geo_id = OG.id_c 
LEFT JOIN kharif_2076.done_khasra_reporting DR ON DR.i_geohierarchy_id_c = OG.id_c 
where OG.geotype_c = 'village' 
AND OG.tehsil_census_c != ''
AND DATE_FORMAT(DE.date_entered, "%m-%Y") <= "10-2019" AND DE.deleted = '0'
group by OG.district_census_c,OG.tehsil_census_c;
/*____________________OR_________________________________OR_________________________________OR________________________________OR____________________________*/

SELECT t2.district_census_c as distId, t2.tehsil_census_c as id,
IFNULL(SUM(KC.khasra_count),0) kul_khasra, 
IFNULL(SUM(DR.done_khasra),0) doneKhasra, 
SUM(CASE WHEN IFNULL(DR.done_khasra,0) > IFNULL(KC.khasra_count,0) THEN IFNULL(KC.khasra_count,0) ELSE IFNULL(DR.done_khasra,0) END) as totalKhasra
FROM lrs.online_geo_data t1 
JOIN lrs.online_geo_data_cstm t2 ON t2.id_c = t1.id 
LEFT JOIN kharif_2076.khasra_count KC ON KC.geo_id = t1.id 
LEFT JOIN kharif_2076.done_khasra_reporting DR ON DR.i_geohierarchy_id_c = t1.id 
WHERE t2.geotype_c = 'village' 
AND DATE_FORMAT(t1.date_entered, "%m-%Y") <= "10-2019" 
AND t1.deleted = '0' AND t2.tehsil_census_c != '' 
GROUP BY t2.district_census_c, t2.tehsil_census_c 
order by t2.district_census_c;
/*========================================================================================================================================================================*/
/*========================================================================================================================================================================*/
/*========================================================================================================================================================================*/
/*========================================================================================================================================================================*/
/*========================================================================================================================================================================*/
/*========================================================================================================================================================================*/
/*========================================================================================================================================================================*/
