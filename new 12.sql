INSERT INTO lrs.online_geo_data(id, name, date_entered, date_modified, deleted) VALUES
('c190dd60-f390-4d06-a74e-066c4b1db1c8', 'पिपरोन', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0'),
('665b54f3-677f-4bdf-9e26-8de6fa71c5fb', 'कोटरा', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0'),
('c483ef51-20c9-4e53-88ad-6610bfc1e993', 'पिपरोन', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '0');

INSERT INTO lrs.online_geo_data_cstm(id_c, creation_date_c, geotype_c, i_geohierarchy_id_c, census_number_c, district_census_c, tehsil_census_c, is_syncing, internal_code) VALUES
('c190dd60-f390-4d06-a74e-066c4b1db1c8', CURRENT_TIMESTAMP, 'patwar', '4caadec0-1c84-4964-a911-ba10bdbe5763', NULL, '106', '00515', '0', 661),
('665b54f3-677f-4bdf-9e26-8de6fa71c5fb', CURRENT_TIMESTAMP, 'village', 'c190dd60-f390-4d06-a74e-066c4b1db1c8', '075435', '106', '00515', '0', 16921),
('c483ef51-20c9-4e53-88ad-6610bfc1e993', CURRENT_TIMESTAMP, 'village', 'c190dd60-f390-4d06-a74e-066c4b1db1c8', '075436', '106', '00515', '0', 16923);



/*SELECT P.patwari_name_c AS name,P.phone_c,P.aadhar_image_c as profilePic,
COUNT(CASE WHEN PC.is_ro_c = 1 THEN 1 WHEN PC.is_sub_ro_c = 1 THEN 1 END) AS done_khasra 
FROM kharif_2076.m_p_13_form_main_cstm PC 
RIGHT JOIN lrs.online_geo_data_cstm GC ON PC.i_geohierarchy_id_c = GC.id_c 
JOIN lrs.m_patwari_cstm P ON P.patwar_circle_c LIKE "%1e1fa168-d93d-432c-ab9d-b7f2dff3b713%"
JOIN lrs.online_geo_data_cstm AC ON AC.census_number_c = GC.tehsil_census_c AND AC.geotype_c = 'tehsil' 
WHERE AC.id_c = '1e1fa168-d93d-432c-ab9d-b7f2dff3b713' AND P.user_type_c IN ('ro','sub_ro') GROUP BY P.id_c;*/
