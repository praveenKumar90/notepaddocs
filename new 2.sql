/*check the total khsras of a patwar circle with village name*/

SELECT oc.name, count(id_c) as total FROM kharif_2076.m_p_13_form_main_cstm 
JOIN lrs.online_geo_data oc ON oc.id = kharif_2076.m_p_13_form_main_cstm.i_geohierarchy_id_c
WHERE m_patwari_id_c = "403d4911-68ef-6ec8-befa-7dbff71e00a6" GROUP by 	i_geohierarchy_id_c;

SELECT o.name, COUNT(t1.id_c) as kharif_girdawari
	FROM lrs.m_patwari_cstm p 
	JOIN kharif_2076.m_p_13_form_main_cstm t1 ON t1.m_patwari_id_c = p.id_c 
    JOIN lrs.online_geo_data o ON o.id = t1.i_geohierarchy_id_c
    WHERE p.id_c = "d40c7763-fb44-fae3-dc52-ce749abcf5bb" GROUP BY o.id;


/* m_p_13_form_main_cstm report in singlw query */

SELECT COUNT(t1.id_c) as kharif_girdawari, COUNT(t2.id_c) as rabi_girdawari, COUNT(t3.id_c) as zaid_girdawari
	FROM lrs.m_patwari_cstm p 
	JOIN kharif_2076.m_p_13_form_main_cstm t1 ON t1.m_patwari_id_c = p.id_c 
    JOIN rabi_2075.m_p_13_form_main_cstm t2 ON t2.m_patwari_id_c = p.id_c
    JOIN zaid_2076.m_p_13_form_main_cstm t3 ON t3.m_patwari_id_c = p.id_c
    WHERE p.id_c = "00355c87-28bb-d39e-3350-0273fbc0afdf";
	
/*kharif_2076 mp13data*/
SELECT o.name, COUNT(t1.id_c) as kharif_girdawari
	FROM lrs.m_patwari_cstm p 
	JOIN kharif_2076.m_p_13_form_main_cstm t1 ON t1.m_patwari_id_c = p.id_c
    JOIN lrs.online_geo_data o ON o.id = t1.i_geohierarchy_id_c
    WHERE p.id_c = "d40c7763-fb44-fae3-dc52-ce749abcf5bb" GROUP BY o.id;
    